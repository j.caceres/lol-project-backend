import json

from django.http import JsonResponse
from django_cassiopeia import cassiopeia as cass
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet


class ChampionView(GenericViewSet):

    @action(detail=False, methods=["GET"], url_path='all_champions')
    def all_champions(self, request):
        champions = cass.Champions(region="EUW")

        serializedChamps = []
        for champion in champions:
            serializedChamps.append({
                "name": champion.name,
                "image": champion.image.url,
                "championId": champion.id,
                "key": champion.key
            })

        return JsonResponse(serializedChamps, safe=False)

    @action(detail=False, methods=["GET"], url_path='champion')
    def champion(self, request, champion_id):
        champion = cass.Champion(id=int(champion_id), region="EUW")
        skins = []

        for number in range(len(champion.skins)):

            skin = {
                "name": champion.skins[number].name,
                "image": champion.skins[number].loading_image_url,
                "splash": champion.skins[number].splash_url,
            }
            skins.append(skin)

        passive = {
            "name": champion.passive.name,
            "image": champion.passive.image_info.url,
            "description": champion.passive.sanitized_description,
        }

        spells = []

        for number in range(4):

            spell = {
                "name": champion.spells[number].name,
                "image": champion.spells[number].image_info.url,
                "description": champion.spells[number].sanitized_description,
                "keywords": champion.spells[number].keywords
            }
            spells.append(spell)

        return JsonResponse({"name": champion.name,
                             "image": champion.image.url,
                             "tags": champion.tags,
                             "play_rates": {
                                 "Top": list(champion.play_rates.values())[0],
                                 "Jungle": list(champion.play_rates.values())[1],
                                 "Middle": list(champion.play_rates.values())[2],
                                 "Bottom": list(champion.play_rates.values())[3],
                                 "Support": list(champion.play_rates.values())[4],
                             },
                             "skins": skins,
                             "ally_tips": champion.ally_tips,
                             "enemy_tips": champion.enemy_tips,
                             "resume": champion.blurb,
                             "passive": passive,
                             "spells": spells
                             })

