import json


def get_paginated_response(match_history):

    loaded_match_history_aux = []
    import random
    for number in range(10):
        loaded_match_history_aux.append(json.loads(match_history[number].load().to_json()))
    random.shuffle(loaded_match_history_aux)
    return loaded_match_history_aux
