import json

from django.http import JsonResponse
from django_cassiopeia import cassiopeia as cass
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet


class ItemView(GenericViewSet):

    @action(detail=False, methods=["GET"], url_path='all_items')
    def all_items(self, request):
        items = cass.get_items(region="EUW")
        loaded_items = []
        for item in items:
            loaded_items.append(json.loads(item.load().to_json()))

        return JsonResponse(loaded_items, safe=False)