import json

from django.http import JsonResponse
from django_cassiopeia import cassiopeia as cass
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet

from apps.views.utils import get_paginated_response


class SummonerView(GenericViewSet):

    @action(detail=False, methods=["GET"], url_path='lite')
    def lite(self, request, summoner_name):
        summoner = cass.Summoner(name=summoner_name, region="EUW")

        return JsonResponse({"name": summoner.name, "level": summoner.level})

    @action(detail=False, methods=["GET"], url_path='full_summoner_data')
    def full_summoner_data(self, request, summoner_name):

        summoner = cass.Summoner(name=summoner_name, region="EUW")
        good_with = summoner.champion_masteries.filter(lambda cm: cm.level >= 6)

        try:
            solo = summoner.league_entries.fives
            solo = {
                "tier": solo.tier.value,
                "division": solo.division.value,
                "wins": solo.wins,
                "losses": solo.losses,
                "points": solo.league_points,
                "name": solo.league.name
            }
        except ValueError as e:
            solo =  {
                   "tier": '',
                   "division": '',
                   "wins": '',
                   "losses": '',
                   "points": '',
                   "name": ''
               },

        try:
            flex = summoner.league_entries.flex
            flex = {
                    "tier": flex.tier.name,
                    "division": flex.division.value,
                    "wins": flex.wins,
                    "losses": flex.losses,
                    "points": flex.league_points,
                    "name": flex.league.name
                }
        except ValueError as e:
            flex =  {
                   "tier": '',
                   "division": '',
                   "wins": '',
                   "losses": '',
                   "points": '',
                   "name": ''
               },

        champions = []
        for cm in good_with:
            champions.append({"name": cm.champion.name,
                              "championId": cm.champion.id,
                              "image": cm.champion.image.url,
                              "tags": cm.champion.tags,
                              })
        loaded_match_history = []
        match_history = cass.get_match_history(continent="EUROPE", region="EUW", platform="europe_west",
                                               puuid=str(summoner.puuid))
        for number in range(10):
            loaded_match_history.append(json.loads(match_history[number].load().to_json()))

        final_object = {
            "name": summoner.name,
            "level": summoner.level,
            "icon": summoner.profile_icon.url,
            "leagues": {
                "solo": solo,
                "flex": flex
            },
            "champions": champions,
            "matchHistory": loaded_match_history
        }

        return JsonResponse(final_object, safe=False)

    @action(detail=False, methods=["PUT"], url_path='match_history')
    def match_history(self, request, summoner_name):
        summoner = cass.Summoner(name=summoner_name, region="EUW")

        initial_match_index = request.data.get("page", 0) * 10 + 1

        match_history = cass.get_match_history(continent="EUROPE", region="EUW", platform="europe_west",
                                                begin_index=initial_match_index, end_index=(initial_match_index+10),
                                                puuid=str(summoner.puuid))

        loaded_match_history = get_paginated_response(match_history)
        return JsonResponse(loaded_match_history, safe=False)

    @action(detail=False, methods=["GET"], url_path='all_summoner_spells')
    def all_summoner_spells(self, request):
        summoner_spells = cass.get_summoner_spells(region="EUW")
        loaded_summoner_spells = []
        for summoner_spell in summoner_spells:
            loaded_summoner_spells.append(json.loads(summoner_spell.load().to_json()))

        return JsonResponse(loaded_summoner_spells, safe=False)

    @action(detail=False, methods=["GET"], url_path='all_runes')
    def all_runes(self, request):
        runes = cass.get_runes(region="EUW")
        loaded_runes = []
        for rune in runes:
            loaded_runes.append(json.loads(rune.load().to_json()))

        return JsonResponse(loaded_runes, safe=False)